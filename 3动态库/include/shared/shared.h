#ifndef INCLUDE_SHARED_H_
#define INCLUDE_SHARED_H_

#ifndef SharedDLL
#   ifdef _WIN32
#       if defined( BUILD_DLL )
#           define SharedDLL __declspec(dllexport)
#       elif defined( USE_DLL )
#           define SharedDLL __declspec(dllimport)
#       else
#           define SharedDLL
#   endif
#   else
#       define SharedDLL
#   endif
#endif


class SharedDLL Shared {
public:
    void print();
};

#endif
