# 建立 static1 子项目

set( source_static1
    ${CMAKE_CURRENT_SOURCE_DIR}/static1.cc
)

add_library( static1 STATIC
    ${source_static1}
)

target_include_directories( static1
    PUBLIC
        ${CMAKE_SOURCE_DIR}/include
)
