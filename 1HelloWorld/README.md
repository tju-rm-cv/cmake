# 1 固定要写的

## 1.1 cmake版本规定

规定 cmake 最低版本，不然会报错

```cmake
cmake_minimum_required(VERSION 3.5)
```

## 1.2 项目名称

没什么卵用，但还必须得写

```cmake
project( hello_world )
```

名称随便写

# 2 基础语法

## 2.1 打印

print的功能，可以用于检查 cmake 是否有问题

```cmake
message(STATUS "hello world")
```

`STATUS` 可以换成 `WARNING`，`FATAL_ERROR`

- `STATUS`：前面会有 `--`，表示正常信息
- `WARNING`：黄色警告
- `FATAL_ERROR`：红色错误，会终止 cmake

## 2.2 变量

### 2.2.1 设置和使用变量的值

```cmake
set(HELLO_WORLD "hello world")
message(STATUS HELLO_WORLD)
message(STATUS ${HELLO_WORLD})
```

1. 第一行是定义一个变量，变量名为 `HELLO_WORLD`，值为 `hello world`
2. 第二行的输出结果是 `HELLO_WORLD`，因为没有加 `${}`，所以只是输出变量名
3. 第三行的输出结果是 `hello world`，因为加了 `${}`，所以输出变量的值

### 2.2.2 cmake内置变量

cmake 内置了很多的变量，比如 `CMAKE_SOURCE_DIR`，`CMAKE_BINARY_DIR`，`CMAKE_CURRENT_SOURCE_DIR`，`CMAKE_CURRENT_BINARY_DIR` 等等

遇到了直接看[文档](https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html)

# 3 创建可执行文件

创建**可执行文件，静态库，动态库**，步骤都是一样的，唯一的区别只是库用的函数不是 `add_executable`，而是 `add_library`，无论是可执行文件，还是静态库，还是动态库，都可以称为 target

1. 添加共享的 include 目录 和链接库目录
   - `include_directories`
   - `link_directories`
2. 设置源文件:
   - `set`
   - `file`
3. 添加可执行文件
   - `add_executable`
4. 添加该可执行文件 的 include 目录 和依赖的链接库
   - `target_include_directories`
   - `target_link_libraries`

## 3.1 添加共享的 include 目录 和链接库目录

这里是为了定义整个项目的 include 目录 和链接库目录，一般都是为了一些很多模块都用到的第三方库

```cmake
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/include /usr/local/cuda/lib)
link_directories( ${CMAKE_CURRENT_SOURCE_DIR}/lib )
```

直接在里面写路径就行了，可以使用 cmake 内置变量

## 3.2 设置源文件

其他步骤里，那些用到的 cmake 函数的参数，都可以直接写具体的文件名，也可以用变量来表示，用变量来表示更方便一些

### set

前面讲了，`set` 可以用来定义基础的字符串，其实还可以定义一个字符串列表的变量：

```cmake
set( source_files main.cc qwq.cc )
```

此时，就定义了一个变量 `source_files`，值为 `main.cc qwq.cc` 的列表

### file

`file` 可以用来自动搜索文件，比如：

```cmake
file(GLOB_RECURSE source_files ${CMAKE_CURRENT_SOURCE_DIR}/*.cc)
```

这个命令会自动搜索`${CMAKE_CURRENT_SOURCE_DIR}`(当前目录)下的所有 `.cc` 文件，然后把结果赋值给 `source_files` 变量

其中，`GLOB_RECURSE` 表示递归搜索，也可以改成 `GLOB` 表示不递归搜索

### set和file比较

- `set` 是手动定义，比较麻烦，每添加一个文件，都要手动修改，但是可以自定义哪些文件不参与编译
- `file` 是自动搜索，比较方便，添加文件基本不用改 cmake，但是会强迫所有符合条件的文件都参与编译

## 3.3 添加可执行文件

```cmake
add_executable( main main.cc )
add_executable( main main.cc qwq.cc )
add_executable( main ${source_files} )
```

- 第一个参数: 可执行文件的名字，之后用 `./main` 来运行
- 后面的参数: 源文件，可以有很多个，也可以直接用变量来表示

## 3.4 添加该可执行文件 的 include 目录 和依赖的链接库

```cmake
target_include_directories( main PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include )
target_link_libraries( main PUBLIC pthread )
```

- 第一个参数: 可执行文件的名字
- 第二个参数: 一共有三种可以选的值，表示当这个 target 被其他 target 引用时，这个 target 链接时，可以使用这个 target 里的哪些东西
  - PRIVATE: 其他 target 不能用 这里生成的 target 引入的东西
  - INTERFACE: 其他 target 只能用 这里生成的 target 引入的东西, 但是不能用这里生成的 target 直接生成的东西 
  - PUBLIC: 都可以用
- 第三个参数：
  - `target_include_directories`：表示 include 目录
  - `target_link_libraries`：表示链接库的名字（下一节会讲）

# 说明

此时，就用以上的知识，以及后面的如何引入第三方库 `find_package`，就可以写一个简单的项目了。只不过，在cmake里没有划分模块。
