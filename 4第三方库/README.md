# 第三方库和cmake(以opencv为例)

## 1 opencv的安装

参考 [教程](https://gitee.com/tju-rm-cv/tju-rm-tutorial/blob/master/docs/07opencv%E5%AE%89%E8%A3%85.md)

## 2 查看 .cmake 文件

### 2.1 .cmake 在哪里

如果是x86_64的电脑，那么opencv的cmake文件应该会在`/usr/lib/x86_64-linux-gnu/cmake`；

如果是arm的电脑，那么opencv的cmake文件应该会在`/usr/lib/aarch64-linux-gnu/cmake`。

PS：具体路径不太确定，但应该大差不差。

那么如果按上面的路径找不到，可以用下面介绍的一个工具来找：文件模糊搜索工具 `fzf`。

1. 安装 `fzf`：`sudo apt install fzf`
2. 进入 `/usr`目录(因为一般装的库都在这下面)：`cd /usr`
3. 使用 `fzf`：`fzf`
   1. 输入`fzf`后，会进入类似下面的界面:
   ![1682653093462](assets/README/1682653093462.png)
   2. 然后此时输入一串字符，`fzf`就会进行模糊搜索
   ![1682653152751](assets/README/1682653152751.png)
   3. 选择目标文件，按`Enter`键后，`fzf`会把路径打印到终端上，然后我们复制路径，进去即可

### 2.2 看哪个文件

刚刚找到了四个文件：
```bash
OpenCVConfig.cmake
OpenCVConfig-version.cmake
OpenCVModules.cmake
OpenCVModules-release.cmake
```

其中，`OpenCVConfig.cmake` 是我们需要看的文件。

一般都是 `"{库的名字}Config.cmake"` 或者 `"{库的名字}-config.cmake"`。

### 2.3 怎么看

我们只需要看前面的注释部分！

![1682653532128](assets/README/1682653532128.png)

截图中，第8行开始，就是我们需要了解的东西，比如：

- `OpenCV_LIBS`：opencv的库文件，也就是之后链接的包
- `OpenCV_INCLUDE_DIRS`：opencv的头文件，也就是之后需要`include`的路径
- ......

## 3 我们怎么写 CMakeLists.txt

### 3.1 find_package

`find_package` 函数的作用就是找到 `.cmake` 文件，然后把里面的变量导入到我们的 CMakeLists.txt 中(就是上面 2.3 中讲的那些变量)。

```cmake
find_package(OpenCV REQUIRED)
```

- 第一个参数：库的名字，因为我们刚刚找到的文件是 `OpenCVConfig.cmake`，所以库的名字就是 `OpenCV`，一个字母都不能错！
- 第二个参数：REQUIRED，可有可无，写上的意思就是如果找不到就报错

### 3.2 利用 find_package 导入的变量写剩余部分

假设当前我们有一个 `target` 叫做 `main`，那么我们可以这样写：

```cmake
target_include_directories(main PUBLIC ${OpenCV_INCLUDE_DIRS})
target_link_libraries(main ${OpenCV_LIBS})
```

没错，这行`cmake`代码里出现的变量就是这么来的！

## 4 如果find_package找不到

这里指的是，我们知道`.cmake`文件在哪里，但是`find_package`找不到。

可能是`.cmake`不在系统默认的路径下，那么我们可以这样写：

如果使用了`REQUIRED`，那么会出现以下报错：

![1682654455489](assets/README/1682654455489.png)

最后一段提示我们可以指定 `${CMAKE_PREFIX_PATH}` 或 `qwq_DIR`，那么我们就可以这样写：

```cmake
set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
```

上面的一行代码用c++解释就是：
```c++
CMAKE_PREFIX_PATH = ${CMAKE_PREFIX_PATH} + "${CMAKE_SOURCE_DIR}/cmake/"
```
