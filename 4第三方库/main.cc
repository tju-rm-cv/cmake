#include <iostream>
#include <opencv2/opencv.hpp>


int main(int argc, char** argv) {
    // 初始化一个opencv的红色矩阵
    cv::Mat image = cv::Mat::zeros(480, 640, CV_8UC3);
    image.setTo(cv::Scalar(0, 0, 255));
    cv::imshow("image", image);
    cv::waitKey(3000);
    return 0;
}
